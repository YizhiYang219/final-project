/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mapeditor.workspaceController;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.transform.Scale;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import mapeditor.MapEditorApp;
import mapeditor.data.DataManager;
import mapeditor.gui.Workspace;
import properties_manager.PropertiesManager;
import static saf.settings.AppPropertyType.BACKGROUND_COLOR;
import static saf.settings.AppPropertyType.BORDERS_COLOR;
import static saf.settings.AppPropertyType.CANCEL;
import static saf.settings.AppPropertyType.CAPITAL;
import static saf.settings.AppPropertyType.CHANGEMAPNAME_TOOLTIP;
import static saf.settings.AppPropertyType.CHANGE_MAPS_DIMENSIONS;
import static saf.settings.AppPropertyType.CHANGE_MAP_NAME;
import static saf.settings.AppPropertyType.EDIT_MAP;
import static saf.settings.AppPropertyType.LOAD_WORK_TITLE;
import static saf.settings.AppPropertyType.ENTER_A_NAME_FOR_THE_MAP;
import static saf.settings.AppPropertyType.LEADER;
import static saf.settings.AppPropertyType.NAME_OF_THE_MAP;
import static saf.settings.AppPropertyType.OK;
import static saf.settings.AppPropertyType.NAME;
import static saf.settings.AppPropertyType.NEXT;
import static saf.settings.AppPropertyType.PICK_A_BACKGROUND_COLOR;
import static saf.settings.AppPropertyType.PICK_A_BORDERS_COLOR;
import static saf.settings.AppPropertyType.PREVIOUS;
import static saf.settings.AppStartupConstants.PATH_WORK;
import static saf.settings.AppPropertyType.SAVE_WORK_TITLE;
import static saf.settings.AppPropertyType.X_COORDINATE;
import static saf.settings.AppPropertyType.Y_COORDINATE;
import static saf.settings.AppStartupConstants.PATH_WORK;

/**
 *
 * @author YizYang
 */
public class WorkspaceController {

    MapEditorApp app;

    public WorkspaceController() {

    }

    public WorkspaceController(MapEditorApp app) {

        this.app = app;
    }

    public void handlerChangeMapName() {

        PropertiesManager props = PropertiesManager.getPropertiesManager();

        GridPane promptForChangeName = new GridPane();

        promptForChangeName.setAlignment(Pos.CENTER);
        promptForChangeName.setVgap(5.5);

        TextField name = new TextField();
        promptForChangeName.add(new Label(props.getProperty(ENTER_A_NAME_FOR_THE_MAP)), 0, 0);
        promptForChangeName.add(name, 1, 0);

        Button ok = new Button(props.getProperty(OK));
        Button cancel = new Button(props.getProperty(CANCEL));

        promptForChangeName.add(ok, 0, 7);
        promptForChangeName.add(cancel, 1, 7);

        Stage stage = new Stage();

        Scene scene = new Scene(promptForChangeName, 350, 200);

        scene.getStylesheets().add("mapeditor/css/mv_style.css");
        promptForChangeName.getStyleClass().add("cust");

        stage.setTitle(props.getProperty(NAME_OF_THE_MAP));

        stage.setScene(scene);

        stage.show();

        DataManager data = (DataManager) app.getDataComponent();
        ok.setOnAction(e -> {

            data.setName(name.getText());

            stage.close();
        }
        );
        cancel.setOnAction(e -> stage.close());
    }

    public void handlerAddImageToMap() throws IOException {

        PropertiesManager props = PropertiesManager.getPropertiesManager();

        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(PATH_WORK));
        fc.setTitle(props.getProperty(LOAD_WORK_TITLE));
        File selectedFile = fc.showOpenDialog(app.getGUI().getWindow());

        Workspace workspace = (Workspace) app.getWorkspaceComponent();

        Pane left = workspace.getLeft();
        // ONLY OPEN A NEW IMAGE IF THE USER SAYS OK
        if (selectedFile != null) {

            BufferedImage bufferedImage = ImageIO.read(selectedFile);

            Image image = SwingFXUtils.toFXImage(bufferedImage, null);

            ImageView imageView = new ImageView(image);

            Group group = workspace.getGroupImages();
            
            group.getChildren().add(imageView);

            workspace.getLeft().getChildren().add(group);
        }
    }

    public void changeBackgroundColor() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        Stage stage = new Stage();
        stage.setTitle(props.getProperty(PICK_A_BACKGROUND_COLOR));
        Scene scene = new Scene(new GridPane(), 300, 300);
        GridPane gridPane = (GridPane) scene.getRoot();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setVgap(10);

        final ColorPicker colorPicker = new ColorPicker();
        colorPicker.setValue(Color.CORNFLOWERBLUE);

        final Text text = new Text(props.getProperty(BACKGROUND_COLOR));
        text.setFont(Font.font("Verdana", 15));
        text.setFill(colorPicker.getValue());

        text.getStyleClass().add("cust");
        Button ok = new Button(props.getProperty(OK));
        Button cancel = new Button(props.getProperty(CANCEL));

        DataManager dataManager = (DataManager) app.getDataComponent();

        colorPicker.setOnAction(e -> {
            text.setFill(colorPicker.getValue());
            ok.setOnAction(ex -> {
                Workspace workspace = (Workspace) app.getWorkspaceComponent();

                Pane left = workspace.getLeft();

                left.setBackground(new Background(new BackgroundFill(colorPicker.getValue(), CornerRadii.EMPTY, Insets.EMPTY)));

                String hex = String.format("#%02X%02X%02X",
                        (int) (colorPicker.getValue().getRed() * 255),
                        (int) (colorPicker.getValue().getGreen() * 255),
                        (int) (colorPicker.getValue().getBlue() * 255));

                dataManager.setBackground(hex);
            });
        });

        ok.setOnAction(ex -> {
            Workspace workspace = (Workspace) app.getWorkspaceComponent();

            Pane left = workspace.getLeft();

            left.setBackground(new Background(new BackgroundFill(colorPicker.getValue(), CornerRadii.EMPTY, Insets.EMPTY)));

            String hex = String.format("#%02X%02X%02X",
                    (int) (colorPicker.getValue().getRed() * 255),
                    (int) (colorPicker.getValue().getGreen() * 255),
                    (int) (colorPicker.getValue().getBlue() * 255));
            dataManager.setBackground(hex);
        });

        gridPane.add(colorPicker, 0, 0);
        gridPane.add(text, 0, 1);
        gridPane.add(ok, 0, 2);
        gridPane.add(cancel, 1, 2);

        scene.getStylesheets().add("mapeditor/css/mv_style.css");
        gridPane.getStyleClass().add("cust");

        stage.setScene(scene);
        stage.show();
    }

    public void changeBorderColor() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        Stage stage = new Stage();

        String title = props.getProperty(PICK_A_BORDERS_COLOR);

        stage.setTitle(title);

        Scene scene = new Scene(new GridPane(), 300, 300);
        GridPane gridPane = (GridPane) scene.getRoot();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setVgap(10);

        final ColorPicker colorPicker = new ColorPicker();
        colorPicker.setValue(Color.CORAL);

        final Text text = new Text(props.getProperty(BORDERS_COLOR));
        text.setFont(Font.font("Verdana", 15));
        text.setFill(colorPicker.getValue());
        text.getStyleClass().add("cust");

        colorPicker.setOnAction(e
                -> text.setFill(colorPicker.getValue())
        );

        Button ok = new Button(props.getProperty(OK));
        Button cancel = new Button(props.getProperty(CANCEL));

        //GridPane.getChildren().addAll(colorPicker, text);
        gridPane.add(colorPicker, 0, 0);
        gridPane.add(text, 0, 1);
        gridPane.add(ok, 0, 2);
        gridPane.add(cancel, 1, 2);

        DataManager dataManager = (DataManager) app.getDataComponent();

        colorPicker.setOnAction(e -> {
            text.setFill(colorPicker.getValue());
            ok.setOnAction(ex -> {
                Workspace workspace = (Workspace) app.getWorkspaceComponent();

                for (int i = 0; i < dataManager.getPolygons().size(); i++) {
                    dataManager.getPolygons().get(i).setStroke(colorPicker.getValue());
                }

                String hex = String.format("#%02X%02X%02X",
                        (int) (colorPicker.getValue().getRed() * 255),
                        (int) (colorPicker.getValue().getGreen() * 255),
                        (int) (colorPicker.getValue().getBlue() * 255));

                dataManager.setBorderColor(hex);
            });
        });

        ok.setOnAction(ex -> {
            for (int i = 0; i < dataManager.getPolygons().size(); i++) {
                dataManager.getPolygons().get(i).setStroke(colorPicker.getValue());
            }

            String hex = String.format("#%02X%02X%02X",
                    (int) (colorPicker.getValue().getRed() * 255),
                    (int) (colorPicker.getValue().getGreen() * 255),
                    (int) (colorPicker.getValue().getBlue() * 255));
            dataManager.setBorderColor(hex);
        });

        scene.getStylesheets().add("mapeditor/css/mv_style.css");
        gridPane.getStyleClass().add("cust");
        stage.setScene(scene);
        stage.show();
    }

    public void changeMapDimensions() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        GridPane promptForChangeDimensions = new GridPane();

        promptForChangeDimensions.setAlignment(Pos.CENTER);
        promptForChangeDimensions.setVgap(5.5);

        TextField X = new TextField();
        promptForChangeDimensions.add(new Label(props.getProperty(X_COORDINATE)), 0, 0);
        promptForChangeDimensions.add(X, 1, 0);

        TextField Y = new TextField();
        promptForChangeDimensions.add(new Label(props.getProperty(Y_COORDINATE)), 0, 1);
        promptForChangeDimensions.add(Y, 1, 1);

        Button ok = new Button(props.getProperty(OK));
        Button cancel = new Button(props.getProperty(CANCEL));

        promptForChangeDimensions.add(ok, 0, 7);
        promptForChangeDimensions.add(cancel, 1, 7);

        Stage stage = new Stage();

        Scene scene = new Scene(promptForChangeDimensions, 350, 200);

        stage.setTitle(props.getProperty(CHANGE_MAPS_DIMENSIONS));

        scene.getStylesheets().add("mapeditor/css/mv_style.css");
        promptForChangeDimensions.getStyleClass().add("cust");
        stage.setScene(scene);

        stage.show();

        ok.setOnAction(e -> stage.close());
        cancel.setOnAction(e -> stage.close());
    }

    public void editSubregion() {

        PropertiesManager props = PropertiesManager.getPropertiesManager();

        GridPane editSubregion = new GridPane();

        editSubregion.setAlignment(Pos.CENTER);
        editSubregion.setVgap(5.5);

        TextField name = new TextField();
        editSubregion.add(new Label(props.getProperty(NAME)), 1, 1);

        editSubregion.add(name, 2, 1);

        TextField capital = new TextField();
        editSubregion.add(new Label(props.getProperty(CAPITAL)), 1, 2);
        editSubregion.add(capital, 2, 2);

        TextField leader = new TextField();
        editSubregion.add(new Label(props.getProperty(LEADER)), 1, 3);
        editSubregion.add(leader, 2, 3);

        //Button previous = new Button(props.getProperty(NEXT));
        //Button next = new Button(props.getProperty(PREVIOUS));
        Button previous = new Button();

        previous.setGraphic(new ImageView(new Image("file:./images/Previous.png")));

        Button next = new Button();
        next.setGraphic(new ImageView(new Image("file:./images/Next.png")));

        editSubregion.add(previous, 1, 5);
        editSubregion.add(next, 4, 5);

        Button ok = new Button(props.getProperty(OK));
        Button cancel = new Button(props.getProperty(CANCEL));

        editSubregion.add(ok, 1, 7);
        editSubregion.add(cancel, 4, 7);

        Scene scene = new Scene(editSubregion, 700, 700);

        Image img = new Image("file:./images/flag.png");

        ImageView imgView = new ImageView(img);

        editSubregion.add(imgView, 0, 0);

        Image img2 = new Image("file:./images/leader.png");

        imgView = new ImageView(img2);

        editSubregion.add(imgView, 4, 0);

        scene.getStylesheets().add("mapeditor/css/mv_style.css");
        editSubregion.getStyleClass().add("cust");

        Stage stage = new Stage();

        stage.setTitle(props.getProperty(EDIT_MAP));

        stage.setScene(scene);

        stage.show();
    }

    public void thicknessHandler(Slider slider, DataManager dataManager) {

        for (int i = 0; i < dataManager.getPolygons().size(); i++) {

            dataManager.getPolygons().get(i).setStrokeWidth(slider.getValue());
        }
    }

    public void zoomHandler(Slider zoom, DataManager dataManager) {

        Workspace workspace = (Workspace) app.getWorkspaceComponent();

        Group map = workspace.getGroupMap();

        map.setScaleX(zoom.getValue());
        map.setScaleY(zoom.getValue());
    }

}

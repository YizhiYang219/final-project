/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mapeditor.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import mapeditor.data.DataManager;
import mapeditor.data.Subregion;
import mapeditor.gui.Workspace;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;

/**
 *
 * @author McKillaGorilla
 */
public class FileManager implements AppFileComponent {

    int orig;
    double origBar;

    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        
        ((DataManager)data).reset();
        loadInfoData((DataManager) data, filePath);
        //loadAndRenderGeographicData(data, filePath);
    }

    public void loadFakeData(DataManager dataManager, String filePath) throws IOException {

        JsonObject json = loadJSONFile(filePath);
    }

    public double getDataAsDouble(JsonObject json, String dataName) {
        JsonValue value = json.get(dataName);
        JsonNumber number = (JsonNumber) value;
        return number.bigDecimalValue().doubleValue();
    }

    public int getDataAsInt(JsonObject json, String dataName) {
        JsonValue value = json.get(dataName);
        JsonNumber number = (JsonNumber) value;
        return number.bigIntegerValue().intValue();
    }

    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    public JsonObject loadJSONFile(String jsonFilePath) throws IOException {

        BufferedReader is = new BufferedReader(new InputStreamReader(new FileInputStream(jsonFilePath), "UTF8"));

        //InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {

        DataManager dataManager = new DataManager();
        File file = new File(filePath);
    }

    public void saveFakeData(String filePath, DataManager dataManager) throws IOException {

        File file = new File(filePath);
        if (!file.exists()) {
            file.createNewFile();
        }

        FileWriter fileWriter = new FileWriter(file);

        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        ObservableList<Subregion> items = dataManager.getSubregions();

        for (Subregion item : items) {

            JsonObject itemJson = Json.createObjectBuilder()
                    .add("name", item.getName())
                    .add("leaderName", item.getLeader())
                    .add("capital", item.getCapital())
                    .add("red", item.getRed())
                    .add("green", item.getGreen())
                    .add("blue", item.getBlue())
                    .add("leader", item.getLeaderImagePath())
                    .add("flag", item.getFlagImagePath()).build();
            arrayBuilder.add(itemJson);
        }
        JsonArray itemsArray = arrayBuilder.build();

        // THEN PUT IT ALL TOGETHER IN A JsonObject
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add("name", dataManager.getName())
                .add("rawData", dataManager.getRawDataPath())
                .add("backgroundColor", dataManager.getBackgroundColor())
                .add("borderColor", dataManager.getBorderColor())
                .add("borderThickness", dataManager.getBorderThickness())
                .add("zoomLevel", dataManager.getZoomLevel())
                .add("XPositionOfNationalFlag", dataManager.getXPositionOfNationalFlag())
                .add("YPositionOfNationalFlag", dataManager.getYPositionOfNationalFlag())
                .add("XPositionOfNationalLogo", dataManager.getXPositionOfNationalLogo())
                .add("YPositionOfNationalLogo", dataManager.getYPositionOfNationalLogo())
                .add("scrollPosition", dataManager.getScrollLocation())
                .add("flagPath", dataManager.getFlagPath())
                .add("logoPath", dataManager.getLogoPath())
                .add("numberOfRegions", itemsArray.size())
                .add("regions", itemsArray)
                .build();

        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();
    }

    @Override
    public void exportData(AppDataComponent dataManager, String filePath) throws IOException {

        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        ObservableList<Subregion> items = ((DataManager) dataManager).getSubregions();

        Boolean capitals = true;
        Boolean flags = true;
        Boolean leaders = true;

        for (int i = 0; i < ((DataManager) dataManager).getSubregions().size(); i++) {

            if (((Subregion) ((DataManager) dataManager).getSubregions().get(i)).getCapital().isEmpty()) {
                capitals = false;

            }
        }

        for (int i = 0; i < ((DataManager) dataManager).getSubregions().size(); i++) {

            File file = new File(((Subregion) ((DataManager) dataManager).getSubregions().get(i)).getFlagImagePath());

            if (!file.exists()) {
                System.out.println(((Subregion) ((DataManager) dataManager).getSubregions().get(i)).getFlagImagePath());
                flags = false;
            }

        }

        for (int i = 0; i < ((DataManager) dataManager).getSubregions().size(); i++) {

            File file = new File(((Subregion) ((DataManager) dataManager).getSubregions().get(i)).getLeaderImagePath());

            if (!file.exists()) {
                
                leaders = false;
            }
        }

        for (Subregion item : items) {

            if (capitals == false && leaders == false) {
                JsonObject itemJson = Json.createObjectBuilder()
                        .add("name", item.getName())
                        .add("red", new Integer(item.getRed()))
                        .add("green", new Integer(item.getGreen()))
                        .add("blue", new Integer(item.getBlue())).build();
                arrayBuilder.add(itemJson);
            } else if (capitals == false && leaders == true) {
                JsonObject itemJson = Json.createObjectBuilder()
                        .add("name", item.getName())
                        .add("leader", item.getLeader())
                        .add("red", new Integer(item.getRed()))
                        .add("green", new Integer(item.getGreen()))
                        .add("blue", new Integer(item.getBlue())).build();
                arrayBuilder.add(itemJson);
            } else if (capitals == true && leaders == false) {
                JsonObject itemJson = Json.createObjectBuilder()
                        .add("name", item.getName())
                        .add("capital", item.getCapital())
                        .add("red", new Integer(item.getRed()))
                        .add("green", new Integer(item.getGreen()))
                        .add("blue", new Integer(item.getBlue())).build();
                arrayBuilder.add(itemJson);

            } else if (capitals == true && leaders == true) {

                JsonObject itemJson = Json.createObjectBuilder()
                        .add("name", item.getName())
                        .add("capital", item.getCapital())
                        .add("leader", item.getLeader())
                        .add("red", new Integer(item.getRed()))
                        .add("green", new Integer(item.getGreen()))
                        .add("blue", new Integer(item.getBlue())).build();
                arrayBuilder.add(itemJson);
            }
        }
        JsonArray itemsArray = arrayBuilder.build();

        // THEN PUT IT ALL TOGETHER IN A JsonObject
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add("name", ((DataManager) dataManager).getName())
                .add("subregions_have_capitals", capitals)
                .add("subregions_have_flags", flags)
                .add("subregions_have_leaders", leaders)
                .add("subregions", itemsArray)
                .build();

        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();
    }

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void loadAndRenderGeographicData(AppDataComponent data, String filePath) throws IOException {

        orig = 0;

        ProgressBar progress = new ProgressBar();
        progress.setPrefWidth(300.0d);
        Label updateLabel = new Label("Running: ");
        updateLabel.setPrefWidth(300.0d);

        VBox updatePane = new VBox();
        updatePane.setPadding(new Insets(10));
        updatePane.setSpacing(5.0d);
        updatePane.getChildren().addAll(updateLabel, progress);

        Stage taskUpdateStage = new Stage(StageStyle.UTILITY);
        taskUpdateStage.setScene(new Scene(updatePane));
        taskUpdateStage.show();

        Task loading;
        loading = new Task<Void>() {
            @Override
            protected Void call() throws Exception {

                DataManager dataManager = (DataManager) data;
                dataManager.reset();

                JsonObject json = FileManager.this.loadJSONFile(filePath);

                int numberOfSubregions = Integer.parseInt(json.get("NUMBER_OF_SUBREGIONS").toString());

                int numberOfPolygons;

                JsonArray arrayOfSubregions = json.getJsonArray("SUBREGIONS");

                double interval = (1.0 / arrayOfSubregions.size()) * 100;

                origBar = interval;

                System.out.println(interval);

                for (int i = 1; i <= arrayOfSubregions.size(); i++) {

                    JsonObject outterObject = arrayOfSubregions.getJsonObject(i - 1);

                    numberOfPolygons = Integer.parseInt(outterObject.get("NUMBER_OF_SUBREGION_POLYGONS").toString());

                    for (int j = 0; j < 1; j++) {

                        JsonArray middleArray = outterObject.getJsonArray("SUBREGION_POLYGONS");

                        for (int k = 0; k < middleArray.size(); k++) {

                            Polygon polygon = new Polygon();

                            JsonArray innerArray = middleArray.getJsonArray(k);

                            for (int z = 0; z < innerArray.size(); z++) {

                                double x = FileManager.this.getDataAsDouble(innerArray.getJsonObject(z), "X");
                                double y = FileManager.this.getDataAsDouble(innerArray.getJsonObject(z), "Y");

                                double relativeX = ((DataManager) data).convertX(x);

                                double relativeY = ((DataManager) data).convertY(y);

                                polygon.getPoints().addAll(relativeX, relativeY);

                            }

                            polygon.setFill(Color.LIGHTGREEN);
                            polygon.setStroke(Color.BLACK);
                            polygon.setStrokeWidth(0.1);
                            ((DataManager) data).addPolygonToList(polygon);

                        }

                    }
                    updateProgress(i, arrayOfSubregions.size());
                    updateMessage("Loading the map: " + String.valueOf((int) origBar) + "%");
                    
                    origBar += interval;

                    orig += 1;

                    Thread.sleep(200);
                }
                return null;
            }
        };

        loading.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent t) {
                taskUpdateStage.hide();
                ((DataManager) data).reloadWorkspace();

            }
        });
        progress.progressProperty().bind(loading.progressProperty());
        updateLabel.textProperty().bind(loading.messageProperty());

        taskUpdateStage.show();
        Thread thread = new Thread(loading);
        thread.start();
    }

    public void loadInfoData(DataManager dataManager, String filePath) throws IOException {

        JsonObject json = loadJSONFile(filePath);

        String rawData = json.getString("rawData");

//        loadAndRenderGeographicData(dataManager, rawData);

        dataManager.setName(json.getString("name"));
        dataManager.setRawData(json.getString("rawData"));
        dataManager.setBackground(json.getString("backgroundColor"));
        dataManager.setBorderColor(json.getString("borderColor"));
        dataManager.setBorderThickness(json.getString("borderThickness"));
        dataManager.setZoomLevel(json.getString("zoomLevel"));
        dataManager.setPositionOfNationalFlag(json.getString("XPositionOfNationalFlag"), json.getString("YPositionOfNationalFlag"));
        dataManager.setPositionOfNationalLogo(json.getString("XPositionOfNationalLogo"), json.getString("YPositionOfNationalLogo"));
        dataManager.setFlag(json.getString("flagPath"));
        dataManager.setLogo(json.getString("logoPath"));
        dataManager.setScrollLoaction(json.getString("scrollPosition"));

        int counter = json.getInt("numberOfRegions");

        JsonArray jsonItemArray = json.getJsonArray("regions");

        for (int i = 0; i < counter; i++) {
            JsonObject jsonItem = jsonItemArray.getJsonObject(i);
            Subregion item = loadItem(jsonItem);
            dataManager.add(item);
        }
        
        loadAndRenderGeographicData(dataManager, rawData);
    }

    public Subregion loadItem(JsonObject jsonItem) {

        String name = jsonItem.getString("name");
        String leaderName = jsonItem.getString("leaderName");
        String capital = jsonItem.getString("capital");
        String red = jsonItem.getString("red");
        String green = jsonItem.getString("green");
        String blue = jsonItem.getString("blue");
        String leader = jsonItem.getString("leader");
        String flag = jsonItem.getString("flag");

        Subregion subregion = new Subregion(name, leaderName, capital, red, green, blue, leader, flag);

        return subregion;
    }

    public void buildMap(DataManager dataManager, String rawData) throws IOException {

        dataManager.reset();

        JsonObject json = this.loadJSONFile(rawData);

        int numberOfSubregions = Integer.parseInt(json.get("NUMBER_OF_SUBREGIONS").toString());

        int numberOfPolygons;

        JsonArray arrayOfSubregions = json.getJsonArray("SUBREGIONS");

        double interval = (1.0 / arrayOfSubregions.size()) * 100;

        origBar = interval;

        System.out.println(interval);

        for (int i = 1; i <= arrayOfSubregions.size(); i++) {

            JsonObject outterObject = arrayOfSubregions.getJsonObject(i - 1);

            numberOfPolygons = Integer.parseInt(outterObject.get("NUMBER_OF_SUBREGION_POLYGONS").toString());

            for (int j = 0; j < 1; j++) {

                JsonArray middleArray = outterObject.getJsonArray("SUBREGION_POLYGONS");

                for (int k = 0; k < middleArray.size(); k++) {

                    Polygon polygon = new Polygon();

                    JsonArray innerArray = middleArray.getJsonArray(k);

                    for (int z = 0; z < innerArray.size(); z++) {

                        double x = FileManager.this.getDataAsDouble(innerArray.getJsonObject(z), "X");
                        double y = FileManager.this.getDataAsDouble(innerArray.getJsonObject(z), "Y");

                        double relativeX = dataManager.convertX(x);

                        double relativeY = dataManager.convertY(y);

                        polygon.getPoints().addAll(relativeX, relativeY);
                    }

                    polygon.setFill(Color.LIGHTGREEN);
                    polygon.setStroke(Color.BLACK);
                    polygon.setStrokeWidth(0.1);
                    dataManager.addPolygonToList(polygon);

                }

            }

        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mapeditor.gui;

import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Polygon;
import javafx.scene.text.Font;
import javafx.scene.transform.Scale;
import mapeditor.MapEditorApp;
import mapeditor.data.DataManager;
import mapeditor.data.Subregion;
import mapeditor.workspaceController.WorkspaceController;
import properties_manager.PropertiesManager;
import saf.components.AppWorkspaceComponent;
import static saf.settings.AppPropertyType.NEW_ICON;
import static saf.settings.AppPropertyType.NEW_TOOLTIP;
import static saf.settings.AppPropertyType.CHANGEMAPNAME_TOOLTIP;
import static saf.settings.AppPropertyType.ADDIMAGETOMAP_TOOLTIP;
import static saf.settings.AppPropertyType.REMOVEIMAGEFROMMAP_TOOLTIP;
import static saf.settings.AppPropertyType.CHANGEMAPBACKGROUNDCOLOR_TOOLTIP;
import static saf.settings.AppPropertyType.CHANGEBORDERCOLOR_TOOLTIP;
import static saf.settings.AppPropertyType.CHANGEBORDERTHICKNESS_TOOLTIP;
import static saf.settings.AppPropertyType.REASSIGNMAPCOLORS_TOOLTIP;
import static saf.settings.AppPropertyType.PLAYSUBREGIONANTHEM_TOOLTIP;
import static saf.settings.AppPropertyType.ZOOMMAPINOUT_TOOLTIP;
import static saf.settings.AppPropertyType.CHANGEMAPDIMENSIONS_TOOLTIP;
import static saf.settings.AppPropertyType.CHANGE_MAP_NAME;
import static saf.settings.AppPropertyType.ADD_IMAGE_TO_MAP;
import static saf.settings.AppPropertyType.REMOVE_IMAGE_ON_MAP;
import static saf.settings.AppPropertyType.CHANGE_MAP_BACKGROUND_COLOR;
import static saf.settings.AppPropertyType.CHANGE_BORDER_COLOR;
import static saf.settings.AppPropertyType.CHANGE_BORDER_THICKNESS;
import static saf.settings.AppPropertyType.REASSIGN_MAP_COLORS;
import static saf.settings.AppPropertyType.PLAY_SUB_REGION_ANTHEM;
import static saf.settings.AppPropertyType.CHANGE_MAP_DIMENSIONS;

import saf.ui.AppGUI;

/**
 *
 * @author McKillaGorilla
 */
public class Workspace extends AppWorkspaceComponent {

    MapEditorApp app;

    AppGUI gui;

    Button changeMapName;
    Button addImageToMap;
    Button removeImageOnMap;
    Button changeMapBackgroundColor;
    Button changeBorderColor;
    Button changeBorderThickness;
    Button reassignMapColors;
    Button playSubregionAnthem;
    Slider thicknessSlider;
    Slider zoomSlider;
    Button changeMapDimensions;
    SplitPane splitPane;

    TableColumn nameColumn;
    TableColumn leaderColumn;
    TableColumn capitalColumn;

    StackPane left;
    TableView<Subregion> itemsTable;

    Group groupForMap;
    Group groupForImages;

    public Workspace(MapEditorApp initApp) {

        app = initApp;

        gui = app.getGUI();

        workspace = new Pane();

        splitPane = new SplitPane();

        left = new StackPane();

        itemsTable = new TableView<>();

        activateWorkspace(app.getGUI().getAppPane());

        layoutGUI();
    }

    @Override
    public void reloadWorkspace() {

        left.getChildren().clear();                                   // add today

        DataManager data = (DataManager) app.getDataComponent();

        ArrayList<Polygon> polygons = data.getPolygons();

        for (int i = 0; i < polygons.size(); i++) {

            //((Pane) splitPane.getItems().get(0)).getChildren().add(polygons.get(i));
            groupForMap.getChildren().add(polygons.get(i));
        }

        left.getChildren().add(groupForMap);

    }

    @Override
    public void initStyle() {

    }

    private void layoutGUI() {

        groupForMap = new Group();

        groupForImages = new Group();

//        left.getChildren().add(groupForMap);
//        left.getChildren().add(groupForImages);

        HBox hbox = new HBox();

        hbox.setMinWidth(100);
        ((FlowPane) app.getGUI().getAppPane().getTop()).getChildren().add(hbox);

        Pane top = (Pane) app.getGUI().getAppPane().getTop();
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        changeMapName = app.getGUI().initChildButton(top, CHANGE_MAP_NAME.toString(), CHANGEMAPNAME_TOOLTIP.toString(), false);
        addImageToMap = app.getGUI().initChildButton(top, ADD_IMAGE_TO_MAP.toString(), ADDIMAGETOMAP_TOOLTIP.toString(), false);
        removeImageOnMap = app.getGUI().initChildButton(top, REMOVE_IMAGE_ON_MAP.toString(), REMOVEIMAGEFROMMAP_TOOLTIP.toString(), false);
        changeMapBackgroundColor = app.getGUI().initChildButton(top, CHANGE_MAP_BACKGROUND_COLOR.toString(), CHANGEMAPBACKGROUNDCOLOR_TOOLTIP.toString(), false);
        changeBorderColor = app.getGUI().initChildButton(top, CHANGE_BORDER_COLOR.toString(), CHANGEBORDERCOLOR_TOOLTIP.toString(), false);
        Label thickness = new Label("  Thickness:   ");
        thickness.getStyleClass().add("cust");
        top.getChildren().add(thickness);
        thicknessSlider = new Slider(0, 0.2, 0.1);
        thicknessSlider.setShowTickLabels(true);
        thicknessSlider.setShowTickMarks(true);
        thicknessSlider.setMajorTickUnit(0.05f);
        thicknessSlider.setBlockIncrement(0.05f);
        //thicknessSlider.setMinWidth(5);
        top.getChildren().add(thicknessSlider);
        Tooltip buttonTooltip = new Tooltip(props.getProperty(CHANGEBORDERTHICKNESS_TOOLTIP.toString()));
        thicknessSlider.setTooltip(buttonTooltip);
        reassignMapColors = app.getGUI().initChildButton(top, REASSIGN_MAP_COLORS.toString(), REASSIGNMAPCOLORS_TOOLTIP.toString(), false);
        playSubregionAnthem = app.getGUI().initChildButton(top, PLAY_SUB_REGION_ANTHEM.toString(), PLAYSUBREGIONANTHEM_TOOLTIP.toString(), false);
        Label zoom = new Label("  Zoom:   ");
        zoom.getStyleClass().add("cust");
        top.getChildren().add(zoom);
        zoomSlider = new Slider(0, 512, 1);
        zoomSlider.setShowTickLabels(true);
        zoomSlider.setShowTickMarks(true);
        zoomSlider.setMajorTickUnit(64f);
        zoomSlider.setBlockIncrement(1f);
        top.getChildren().add(zoomSlider);
        Tooltip buttonTooltip2 = new Tooltip(props.getProperty(ZOOMMAPINOUT_TOOLTIP.toString()));
        zoomSlider.setTooltip(buttonTooltip2);

        changeMapDimensions = app.getGUI().initChildButton(top, CHANGE_MAP_DIMENSIONS.toString(), CHANGEMAPDIMENSIONS_TOOLTIP.toString(), false);

        WorkspaceController workspaceController = new WorkspaceController(app);

        changeMapName.setOnAction(e -> workspaceController.handlerChangeMapName());                                // DONE

        addImageToMap.setOnAction(e -> {                                                                           // Relative path issue and zoom issue.
            try {
                workspaceController.handlerAddImageToMap();
            } catch (IOException ex) {
                Logger.getLogger(Workspace.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        changeMapBackgroundColor.setOnAction(e -> {
            workspaceController.changeBackgroundColor();

        });

        changeBorderColor.setOnAction(e -> workspaceController.changeBorderColor());

        changeMapDimensions.setOnAction(e -> workspaceController.changeMapDimensions());

        thicknessSlider.valueProperty().addListener(e -> {
            workspaceController.thicknessHandler(thicknessSlider, (DataManager) app.getDataComponent());
        });

        zoomSlider.valueProperty().addListener(e -> {
            workspaceController.zoomHandler(zoomSlider, (DataManager) app.getDataComponent());
        });

        
        
        Delta dragDelta = new Delta();
        FlowPane flow = (FlowPane) app.getGUI().getAppPane().getTop();
        
        groupForImages.setOnMouseClicked(e -> {
            System.out.println("check");
            dragDelta.x = groupForImages.getLayoutX() - e.getSceneX();
            dragDelta.y = groupForImages.getLayoutY() - e.getSceneY();
            groupForImages.setCursor(Cursor.MOVE);

        });

        groupForImages.setOnMouseReleased(e -> {
            groupForImages.setCursor(Cursor.HAND);
        });

        groupForImages.setOnMouseDragged(e -> {

            groupForImages.setLayoutX(e.getSceneX() + dragDelta.x);
            groupForImages.setLayoutY(e.getScreenY() + dragDelta.y - top.getHeight());
        });
        
        groupForImages.setOnMouseEntered(e->{
            groupForImages.setCursor(Cursor.HAND);
        });
        
        groupForImages.setOnMouseExited(e -> {
            groupForImages.setCursor(null);
        });
        
        
        
        
        
        nameColumn = new TableColumn("Name");
        leaderColumn = new TableColumn("Leader");
        capitalColumn = new TableColumn("Capital");

        nameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("name"));
        leaderColumn.setCellValueFactory(new PropertyValueFactory<String, String>("leader"));
        capitalColumn.setCellValueFactory(new PropertyValueFactory<String, String>("capital"));
        DataManager dataManager = (DataManager) app.getDataComponent();

        itemsTable.getColumns().add(nameColumn);
        itemsTable.getColumns().add(leaderColumn);
        itemsTable.getColumns().add(capitalColumn);

        itemsTable.setItems(dataManager.getSubregions());

        itemsTable.getStyleClass().add("cust");

        splitPane.setDividerPositions(0.75);

        left.getStyleClass().add("cust");

        itemsTable.getStyleClass().add("cust");

        splitPane.setMinSize(app.getGUI().getPrimaryScene().getWidth(), app.getGUI().getPrimaryScene().getHeight());

        splitPane.getItems().addAll(left, itemsTable);
        workspace.getChildren().add(splitPane);

        itemsTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
    }

    public SplitPane getSplitPane() {

        return splitPane;
    }

    public Pane getLeft() {

        return left;
    }

    public Group getGroupMap() {

        return groupForMap;
    }

    public Group getGroupImages() {

        return groupForImages;
    }

    //        groupForMap.setOnMouseReleased(e -> {
//
//            if (e.getClickCount() == 2) {
//                workspaceController.editSubregion();
//            } else {
//
//                FlowPane flow = (FlowPane) app.getGUI().getAppPane().getTop();
//
//                groupForMap.setTranslateX(groupForMap.getTranslateX() + left.getWidth() / 2 - e.getSceneX());
//                groupForMap.setTranslateY(groupForMap.getTranslateY() + left.getHeight() / 2 + flow.getHeight() - e.getSceneY());
//
//                Scale scale = new Scale();
//
//                scale.setPivotX(e.getX());
//                scale.setPivotY(e.getY());
//                scale.setX(3.5);
//                scale.setY(3.5);
//                groupForMap.getTransforms().add(scale);
//
//                DataManager dataManager = (DataManager) app.getDataComponent();
//
//            }
//        });
    class Delta {

        double x, y;
    }
}

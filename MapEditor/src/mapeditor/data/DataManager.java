package mapeditor.data;

import java.awt.Image;
import java.io.File;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
//import javafx.scene.image.Image;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javax.json.JsonValue;
import mapeditor.MapEditorApp;
import mapeditor.gui.Workspace;
import saf.components.AppDataComponent;

/**
 *
 * @author McKillaGorilla
 */
public class DataManager implements AppDataComponent {

    MapEditorApp app;

    ObservableList<Subregion> subregions;

    ArrayList<Polygon> polygons;

    String name;

    String zoomLevel;

    String backgroundColor;

    String borderColor;

    String borderThickness;

    File rawData;
    File flag;
    File logo;

    String XPositionOfNationalFlag;
    String YPositionOfNationalFlag;

    String XPositionOfNationalLogo;
    String YPositionOfNationalLogo;

    String scrollLocation;
   

    public DataManager() {
        subregions = FXCollections.observableArrayList();

        polygons = new ArrayList<Polygon>();

    }

    public DataManager(MapEditorApp initApp) {

        app = initApp;

        subregions = FXCollections.observableArrayList();

        polygons = new ArrayList<Polygon>();

    }

    @Override
    public void reset() {

        name = "";

        zoomLevel = "";

        backgroundColor = "";

        borderColor = "";

        borderThickness = "";

        rawData = null;
        flag = null;
        logo = null;

        XPositionOfNationalFlag = "";
        YPositionOfNationalFlag = "";

        XPositionOfNationalLogo = "";
        YPositionOfNationalLogo = "";

        scrollLocation = "";

        subregions.clear();
        
        polygons.clear();
        
        //((Workspace)app.getWorkspaceComponent()).getLeft().getChildren().clear();
    }

    public ObservableList getSubregions() {

        return subregions;
    }

    public void add(Subregion subregion) {

        subregions.add(subregion);
    }

    public double convertX(double x) {

        Workspace pane = (Workspace) app.getWorkspaceComponent();

        Double width = ((Pane) ((Workspace) app.getWorkspaceComponent()).getSplitPane().getItems().get(0)).getWidth();

        double relativeX = (((width) / 360.0) * (180 + x));

        return relativeX;
    }

    public double convertY(double y) {

        Workspace pane = (Workspace) app.getWorkspaceComponent();

        FlowPane flow = (FlowPane) app.getGUI().getAppPane().getTop();

        double relativeY = (((app.getGUI().getPrimaryScene().getHeight() - flow.getHeight()) / 180) * (90 - y));

        return relativeY;
    }

    public void addPolygonToList(Polygon polygon) {

        polygons.add(polygon);
    }

    public ArrayList<Polygon> getPolygons() {

        return polygons;
    }

    // THE FOLLOWING METHODS ARE JUST MADE. 
    public void setZoomLevel(String zoomLevel) {

        this.zoomLevel = zoomLevel;
    }

    public void setBackground(String backgroundColor) {

        this.backgroundColor = backgroundColor;
    }

    public void setBorderColor(String borderColor) {

        this.borderColor = borderColor;
    }

    public void setBorderThickness(String thickness) {

        borderThickness = thickness;
    }

    public void setFlag(String path) {

        flag = new File(path);

    }

    public void setLogo(String path) {

        logo = new File(path);
    }

    public void setPositionOfNationalFlag(String X, String Y) {

        XPositionOfNationalFlag = X;
        YPositionOfNationalFlag = Y;
    }

    public void setPositionOfNationalLogo(String X, String Y) {

        XPositionOfNationalLogo = X;
        YPositionOfNationalLogo = Y;
    }

    public void setRawData(String path) {

        rawData = new File(path);
    }

    public String getBackgroundColor() {

        return backgroundColor;
    }

    public String getBorderColor() {

        return borderColor;
    }

    public String getBorderThickness() {

        return borderThickness;
    }

    public String getZoomLevel() {

        return zoomLevel;
    }

    public String getRawDataPath() {

        return rawData.getPath();
    }

    public String getFlagPath() {

        return flag.getPath();
    }

    public String getLogoPath() {

        return logo.getPath();
    }

    public String getXPositionOfNationalFlag() {

        return XPositionOfNationalFlag;
    }

    public String getYPositionOfNationalFlag() {

        return YPositionOfNationalFlag;
    }

    public String getXPositionOfNationalLogo() {

        return XPositionOfNationalLogo;
    }

    public String getYPositionOfNationalLogo() {

        return YPositionOfNationalLogo;
    }

    public void reloadWorkspace() {

        app.getWorkspaceComponent().reloadWorkspace();
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setScrollLoaction(String value) {

        scrollLocation = value;
    }

    public String getScrollLocation() {

        return scrollLocation;
    }
}
